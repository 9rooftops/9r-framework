<h2>Button</h2>
<button class="nine-button">Default</button>

<h2>Button -primary</h2>
<button class="nine-button -primary">Default</button>

<h2>Button -secondary</h2>
<button class="nine-button -secondary">Default</button>

<h2>Button -tertiary</h2>
<button class="nine-button -tertiary">Default</button>
