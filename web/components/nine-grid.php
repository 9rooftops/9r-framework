<h3>Example A: Two columns with gutters that are both 50% in width.</h3>
<code>
<pre>
&lt;div class=&quot;nine-g&quot;&gt;
  &lt;div class=&quot;nine-u-12-24&quot;&gt;Left&lt;/div&gt;
  &lt;div class=&quot;nine-u-12-24&quot;&gt;Right&lt;/div&gt;
&lt;/div&gt;
</pre>
</code>
<div class="nine-g -debug">
  <div class="nine-u-12-24"><div class="l-box">Left</div></div>
  <div class="nine-u-12-24"><div class="l-box">Right</div></div>
</div>
<hr />

<h3>Example B: Three columns that are both 33.333% in width.</h3>
<code>
<pre>
&lt;div class=&quot;nine-g&quot;&gt;
 &lt;div class=&quot;nine-u-8-24&quot;&gt;Left&lt;/div&gt;
 &lt;div class=&quot;nine-u-8-24&quot;&gt;Center&lt;/div&gt;
 &lt;div class=&quot;nine-u-8-24&quot;&gt;Right&lt;/div&gt;
&lt;/div&gt;
</pre>
</code>
<div class="nine-g -debug">
  <div class="nine-u-8-24">Left</div>
  <div class="nine-u-8-24">Center</div>
  <div class="nine-u-8-24">Right</div>
</div>
<hr />

<h3>Responsive Units - 4 columns on large and up screens</h3>
<code><pre>
&lt;div class=&quot;nine-g -debug&quot;&gt;
  &lt;div class=&quot;nine-u-1-1 nine-u-lg-6-24&quot;&gt;&lt;div class=&quot;l-box&quot;&gt;left&lt;/div&gt;&lt;/div&gt;
  &lt;div class=&quot;nine-u-1-1 nine-u-lg-6-24&quot;&gt;&lt;div class=&quot;l-box&quot;&gt;left-center&lt;/div&gt;&lt;/div&gt;
  &lt;div class=&quot;nine-u-1-1 nine-u-lg-6-24&quot;&gt;&lt;div class=&quot;l-box&quot;&gt;right-center&lt;/div&gt;&lt;/div&gt;
  &lt;div class=&quot;nine-u-1-1 nine-u-lg-6-24&quot;&gt;&lt;div class=&quot;l-box&quot;&gt;right&lt;/div&gt;&lt;/div&gt;
&lt;/div&gt;
</pre></code>
<div class="nine-g -debug">
  <div class="nine-u-1-1 nine-u-lg-6-24"><div class="l-box">left</div></div>
  <div class="nine-u-1-1 nine-u-lg-6-24"><div class="l-box">left-center</div></div>
  <div class="nine-u-1-1 nine-u-lg-6-24"><div class="l-box">right-center</div></div>
  <div class="nine-u-1-1 nine-u-lg-6-24"><div class="l-box">right</div></div>
</div>

<div class="nine-section">
  <div class="nine-section__content">
    <div class="nine-section__items nine-g">
      <div class="nine-section__item nine-u-1-1">
        <div class="nine-g -debug -column">
          <div class="nine-u-1-24"><div class="l-box">nine-u-1-24</div></div>
          <div class="nine-u-2-24"><div class="l-box">nine-u-2-24</div></div>
          <div class="nine-u-3-24"><div class="l-box">nine-u-3-24</div></div>
          <div class="nine-u-4-24"><div class="l-box">nine-u-4-24</div></div>
          <div class="nine-u-5-24"><div class="l-box">nine-u-5-24</div></div>
          <div class="nine-u-6-24"><div class="l-box">nine-u-6-24</div></div>
          <div class="nine-u-7-24"><div class="l-box">nine-u-7-24</div></div>
          <div class="nine-u-8-24"><div class="l-box">nine-u-8-24</div></div>
          <div class="nine-u-9-24"><div class="l-box">nine-u-9-24</div></div>
          <div class="nine-u-10-24"><div class="l-box">nine-u-10-24</div></div>
          <div class="nine-u-11-24"><div class="l-box">nine-u-11-24</div></div>
          <div class="nine-u-12-24"><div class="l-box">nine-u-12-24</div></div>
          <div class="nine-u-13-24"><div class="l-box">nine-u-13-24</div></div>
          <div class="nine-u-14-24"><div class="l-box">nine-u-14-24</div></div>
          <div class="nine-u-15-24"><div class="l-box">nine-u-15-24</div></div>
          <div class="nine-u-16-24"><div class="l-box">nine-u-16-24</div></div>
          <div class="nine-u-17-24"><div class="l-box">nine-u-17-24</div></div>
          <div class="nine-u-18-24"><div class="l-box">nine-u-18-24</div></div>
          <div class="nine-u-19-24"><div class="l-box">nine-u-19-24</div></div>
          <div class="nine-u-20-24"><div class="l-box">nine-u-20-24</div></div>
          <div class="nine-u-21-24"><div class="l-box">nine-u-21-24</div></div>
          <div class="nine-u-22-24"><div class="l-box">nine-u-22-24</div></div>
          <div class="nine-u-23-24"><div class="l-box">nine-u-23-24</div></div>
          <div class="nine-u-24-24"><div class="l-box">nine-u-24-24</div></div>
          <div class="nine-u-1-1"><div class="l-box">nine-u-1-1</div></div>
        </div>
      </div>
    </div>
  </div>
</div>
