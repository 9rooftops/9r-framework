<div class="nine-cta">
  <div class="nine-cta__image">
    <img src="https://fastly.picsum.photos/id/221/1920/1080.jpg?grayscale&hmac=GWPBzHGEhEh-BrPn1i-PuximCxLtUpKHcNwyACiTRHk" alt="CTA alt tag for image" />
  </div>
  <style>
    .nine-cta {
      background-image: url('https://fastly.picsum.photos/id/280/720/405.jpg?grayscale&hmac=KjG152dDE2E7wA8RUHhwejGmj_fMIFDX0b07GjeiV4w');
    }
    @media only screen and (min-width: 64em) {
      .nine-cta {
        background-image: url('https://fastly.picsum.photos/id/221/1920/1080.jpg?grayscale&hmac=GWPBzHGEhEh-BrPn1i-PuximCxLtUpKHcNwyACiTRHk');
      }
    }
  </style>
  <div class="nine-cta__content nine-g" data-aos="fade-up">
    <div class="nine-cta__copy nine-u-1-1 nine-u-sm-1-1 nine-u-md-1-1 nine-u-lg-8-24">
      <div class="nine-cta__title">
        <h2>CTA Title</h2>
      </div>
      <div class="nine-cta__body">
        <p>CTA body copy</p>
        <div class="nine-cta__links nine-g -x-center">
          <div class="nine-cta__link">
            <a href="#" class="nine-button -primary">CTA Link 1</a>
          </div>
          <div class="nine-cta__link">
            <a href="#" class="nine-button -secondary">CTA Link 2</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
