<div class="nine-card">
  <div class="nine-card__content">
    <div class="nine-card__items nine-g">
      <div class="nine-card__item l-box nine-u-1-1 nine-u-md-12-24" data-aos="fade-up">
        <button class="nine-card__open-close" type="button" aria-label="Click to show or hide more card details.">
          <i class="far fa-regular far fa-compress"></i>
        </button>
        <div class="nine-card__body">
          <div class="nine-card__image">
            <img width="768" height="287" src="https://picsum.photos/500/300" class="nine-image" alt="">
          </div>
          <div class="nine-card__title h2 l-box">
            title 1
          </div>
          <div class="nine-card__sub-title h3 l-box">
            <strong>sub title 1</strong>
          </div>
          <div class="nine-card__copy l-box">
            copy 1
          </div>
          <div class="nine-card__links nine-g l-box">
            <div class="nine-card__link">
              <a href="#" target="_self">Contact Us <i class="fa-solid fa-caret-right"></i></a>
            </div>
          </div>
          <div class="nine-card__reveal" aria-hidden="true">
            <div class="nine-card__title l-box">
              <h2>Title 1</h2>
            </div>
            <div class="nine-card__sub-title l-box">
              <strong>sub title 1</strong>
            </div>
            <div class="nine-card__copy l-box">
              copy 1
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
