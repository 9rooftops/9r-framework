<div class="nine-accordion">
  <div class="nine-accordion__items">
    <details class="nine-accordion__item">
      <summary class="nine-g -y-center">
        <div class="nine-accordion__title nine-u-20-24">Header Name</div>
        <div class="nine-accordion__open-close nine-u-4-24 -text-right">
          <i class="opened far fa-regular far fa-expand"></i>
          <i class="closed far fa-regular far fa-compress"></i>
        </div>
      </summary>
      <div class="nine-accordion__copy">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      </div>
    </details>
  </div>
</div>
