<div class="nine-accordion" role="tablist">
  <div class="nine-accordion__items">
    <div class="nine-accordion__item">
      <button class="nine-accordion__header nine-g" type="button" role="tab" aria-selected="false" aria-expanded="false" aria-controls="header_name01">
        <div class="nine-accordion__title nine-u-20-24">Header Name</div>
        <i class="opened far fa-regular far fa-expand nine-u-4-24"></i>
        <i class="closed far fa-regular far fa-compress nine-u-4-24"></i>
      </button>
      <div id="header_name01" class="nine-accordion__copy" aria-hidden="true"">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      </div>
    </div>
  </div>
</div>
