<blockquote class="nine-blockquote">
  <div class="nine-blockquote__content nine-g -x-center" data-aos="fade-in">
    <div class="nine-blockquote__icon"><i class="fa-regular fa-message-quote"></i></div>
    <div class="nine-blockquote__person nine-g -x-center l-box">
      <div class="nine-blockquote__image">
        <img src="https://www.pinkvilla.com/images/2022-09/avengers-endgame-star-mark-ruffalo-celebrates-bruce-banners-50th-birthday-with-an-endearing-post.jpg" alt="Bruce Banner" />
      </div>
      <div class="nine-blockquote__name-title">
        <span class="nine-blockquote__name">Bruce Banner</span>
        <span class="nine-blockquote__title">, Avenger</span>
      </div>
      <div class="nine-blockquote__comment">
        <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      </div>
    </div>
  </div>
</blockquote>
<script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "Quotation",
  "creator": {
    "@type": "Person",
    "name": "PERSONS NAME GOES HERE"
  },
  "text": "COMMENT COPY GOES HERE"
}
</script>
