<div class="nine-banner" data-aos="fade-in" style="background-image: url(https://fastly.picsum.photos/id/280/720/405.jpg?grayscale&hmac=KjG152dDE2E7wA8RUHhwejGmj_fMIFDX0b07GjeiV4w);">
  <div class="nine-banner__image">
  <img
    sizes="(max-width: 720px) 100vw 720px"
    srcset="
      https://fastly.picsum.photos/id/672/320/180.jpg?grayscale&hmac=hS-tIhbIFbohP79ALMtOii4mJHTKHSV3FKGU7QWxA7Y 320w
      https://fastly.picsum.photos/id/280/720/405.jpg?grayscale&hmac=KjG152dDE2E7wA8RUHhwejGmj_fMIFDX0b07GjeiV4w 720w"
    src="https://fastly.picsum.photos/id/280/720/405.jpg?grayscale&hmac=KjG152dDE2E7wA8RUHhwejGmj_fMIFDX0b07GjeiV4w" class="nine-img" alt="Banner image description" />
  </div>
</div>
