<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

<div class="nine-alternating" role="region">
  <div class="nine-alternating__items nine-g">
    <div class="nine-alternating__item nine-u-1-1">
      <div class="nine-alternating__content nine-g">
        <div class="nine-alternating__image nine-u-1-1 nine-u-sm-1-1 nine-u-md-1-1 nine-u-lg-12-24" data-aos="fade-up">
          <img src="https://fastly.picsum.photos/id/280/720/405.jpg?grayscale&hmac=KjG152dDE2E7wA8RUHhwejGmj_fMIFDX0b07GjeiV4w" class="nine-img" alt="" />
        </div>
        <div class="nine-alternating__copy l-box nine-u-1-1 nine-u-sm-1-1 nine-u-md-1-1 nine-u-lg-12-24">
          <div class="nine-alternating__title">
            <h2>Alternating title</h2>
          </div>
          <div class="nine-alternating__body">
            <p>Alternating body copy</p>
            <div class="nine-alternating__links">
              <div class="nine-alternating__link">
                <a href="#" class="nine-button">Alternating Link 1</a>
              </div>
              <div class="nine-alternating__link">
                <a href="#" class="nine-button">Alternating Link 2</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="nine-alternating__item nine-u-1-1">
      <div class="nine-alternating__content nine-g">
        <div class="nine-alternating__image nine-u-1-1 nine-u-sm-1-1 nine-u-md-1-1 nine-u-lg-12-24" data-aos="fade-up">
          <img src="https://fastly.picsum.photos/id/280/720/405.jpg?grayscale&hmac=KjG152dDE2E7wA8RUHhwejGmj_fMIFDX0b07GjeiV4w" class="nine-img" alt="Alternating image description" />
        </div>
        <div class="nine-alternating__copy l-box nine-u-1-1 nine-u-sm-1-1 nine-u-md-1-1 nine-u-lg-12-24">
          <div class="nine-alternating__title">
            <h2>Alternating title</h2>
          </div>
          <div class="nine-alternating__body">
            <p>Alternating body copy</p>
            <div class="nine-alternating__links">
              <div class="nine-alternating__link">
                <a href="#" class="nine-button">Alternating Link 1</a>
              </div>
              <div class="nine-alternating__link">
                <a href="#" class="nine-button">Alternating Link 2</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

