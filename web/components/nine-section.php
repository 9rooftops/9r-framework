<section class="nine-section -full">
  <div class="nine-section__content -full">
    <div class="nine-section__title l-box">Section without max width set</div>
    <div class="nine-section__items nine-g">
      <div class="nine-section__item nine-u-1-1 l-box">
        Section background color will alternate.
      </div>
    </div>
  </div>
</section>

<section class="nine-section">
  <div class="nine-section__content -contained">
    <div class="nine-section__title l-box">Section "contained" with max width set</div>
    <div class="nine-section__items nine-g">
      <div class="nine-section__item nine-u-1-1 nine-u-lg-8-24 l-box">
        Section Item goes here. This item can be any of the other components.
      </div>
      <div class="nine-section__item nine-u-1-1 nine-u-lg-8-24 l-box">
        Section Item goes here. This item can be any of the other components.
      </div>
      <div class="nine-section__item nine-u-1-1 nine-u-lg-8-24 l-box">
        Section Item goes here. This item can be any of the other components.
      </div>
    </div>
  </div>
</section>


