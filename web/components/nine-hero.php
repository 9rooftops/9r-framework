<section class="nine-hero" aria-label="Introduction">
  <div class="nine-hero__image">
    <img
      sizes="(max-width: 1440px) 100vw 1440px"
      srcset="
      https://fastly.picsum.photos/id/672/320/180.jpg?grayscale&hmac=hS-tIhbIFbohP79ALMtOii4mJHTKHSV3FKGU7QWxA7Y 320w
        https://fastly.picsum.photos/id/280/720/405.jpg?grayscale&hmac=KjG152dDE2E7wA8RUHhwejGmj_fMIFDX0b07GjeiV4w 720w,
        https://fastly.picsum.photos/id/221/1920/1080.jpg?grayscale&hmac=GWPBzHGEhEh-BrPn1i-PuximCxLtUpKHcNwyACiTRHk 1440w"
      src="https://fastly.picsum.photos/id/221/1920/1080.jpg?grayscale&hmac=GWPBzHGEhEh-BrPn1i-PuximCxLtUpKHcNwyACiTRHk" class="nine-img" alt="Hero image description" />
  </div>
  <div class="nine-hero__content nine-g" data-aos="fade-in">
    <div class="nine-hero__copy nine-u-1-1 nine-u-lg-12-24">
      <div class="nine-hero__title">
        <h1>Hero Title</h1>
      </div>
      <div class="nine-hero__body">
        <p>Hero Body Copy</p>
        <div class="nine-hero__links nine-g">
          <div class="nine-hero__link">
            <a href="#" class="nine-button -primary">Hero Link 1</a>
          </div>
          <div class="nine-hero__link">
            <a href="#" class="nine-button -secondary">Hero Link 2</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<style>
  .nine-hero__image {
    background-image: url('https://fastly.picsum.photos/id/672/320/180.jpg?grayscale&hmac=hS-tIhbIFbohP79ALMtOii4mJHTKHSV3FKGU7QWxA7Y');
  }
  @media only screen and (min-width: 64em) {
    .nine-hero__image {
      background-image: url('https://fastly.picsum.photos/id/221/1920/1080.jpg?grayscale&hmac=GWPBzHGEhEh-BrPn1i-PuximCxLtUpKHcNwyACiTRHk');
    }
  }
</style>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
