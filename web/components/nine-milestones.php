<div class="nine-milestones">
  <div class="nine-milestones__content">
    <div class="nine-milestones__items nine-g"><!-- milestones loop inside items -->

      <div class="nine-milestones__itemnine-u-12-24 nine-u-lg-8-24 l-box" data-aos="fade-up">
        <div class="nine-milestones__body l-box">
          <div class="nine-milestones__percentage" data-number="70"><span class="number">70</span>%</div>
          <div class="nine-milestones__title">Title</div>
          <div class="nine-milestones__subtitle">Subtitle</div>
          <div class="nine-milestones__copy">Lorem ipsum dolor sit amet</div>
          <div class="nine-milestones__link"><a href="#" class="nine-button">Link</a></div>
        </div>
      </div>

      <div class="nine-milestones__itemnine-u-12-24 nine-u-lg-8-24 l-box" data-aos="fade-up">
        <div class="nine-milestones__body l-box">
          <div class="nine-milestones__percentage" data-number="80"><span class="number">70</span>%</div>
          <div class="nine-milestones__title">Title</div>
          <div class="nine-milestones__subtitle">Subtitle</div>
          <div class="nine-milestones__copy">Lorem ipsum dolor sit amet</div>
          <div class="nine-milestones__link"><a href="#" class="nine-button">Link</a></div>
        </div>
      </div>

      <div class="nine-milestones__itemnine-u-12-24 nine-u-lg-8-24 l-box" data-aos="fade-up">
        <div class="nine-milestones__body l-box">
          <div class="nine-milestones__percentage" data-number="90"><span class="number">70</span>%</div>
          <div class="nine-milestones__title">Title</div>
          <div class="nine-milestones__subtitle">Subtitle</div>
          <div class="nine-milestones__copy">Lorem ipsum dolor sit amet</div>
          <div class="nine-milestones__link"><a href="#" class="nine-button">Link</a></div>
        </div>
      </div>

    </div>
  </div>
</div>
