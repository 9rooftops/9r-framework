<div class="nine-tabs" data-id="001">
  <div class="nine-tabs__content nine-g">
    <div class="nine-tabs__navigation nine-u-1-1 l-box">
      <div class="nine-tabs__navigation -mobile -y-center">
        <div class="nine-tabs__placeholder nine-u-20-24" data-id="001">Link 1</div>
        <button class="nine-tabs__menu-open nine-u-4-24" data-id="001" type="button"><i class="fas fa-ellipsis-v"><span class="visually-hidden">Click to open tab menu</span></i></button>
      </div>
      <div class="nine-tabs__menu nine-g -y-center" role="tablist">
        <button class="nine-tabs__menu-item -active" data-id="001" id="tab-1" type="button" role="tab" aria-selected="true" aria-controls="tabpanel-1" >Link 1</button>
        <button class="nine-tabs__menu-item" data-id="001" id="tab-2" type="button" role="tab" aria-selected="false" aria-controls="tabpanel-2" >Link 2</button>
      </div>
    </div>
    <div class="nine-tabs__items nine-g">
      <div class="nine-tabs__item nine-u-1-1 l-box -active" role="tabpanel" aria-hidden="false" id="tabpanel-1" aria-labelledby="tab-1">
        Tab copy 1
        <div class="nine-tabs__links nine-g">
          <div class="nine-tabs__link">
            <a href="#" target="_self'" class="nine-button">Link</a>
          </div>
        </div>
      </div>
      <div class="nine-tabs__item nine-u-1-1 l-box" role="tabpanel" aria-hidden="true" id="tabpanel-2" aria-labelledby="tab-2">
        Tab copy 2
        <div class="nine-tabs__links nine-g">
          <div class="nine-tabs__link">
            <a href="#" target="_self'" class="nine-button">Link</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="nine-tabs" data-id="002">
  <div class="nine-tabs__content nine-g">
    <div class="nine-tabs__navigation nine-u-1-1 l-box">
      <div class="nine-tabs__navigation -mobile -y-center">
        <div class="nine-tabs__placeholder nine-u-20-24" data-id="002">Link 1</div>
        <button class="nine-tabs__menu-open nine-u-4-24" data-id="002" type="button"><i class="fas fa-ellipsis-v"><span class="visually-hidden">Click to open tab menu</span></i></button>
      </div>
      <div class="nine-tabs__menu nine-g -y-center" role="tablist">
        <button class="nine-tabs__menu-item -active" data-id="002" id="tab-1" type="button" role="tab" aria-selected="true" aria-controls="tabpanel-1" >Link 1</button>
        <button class="nine-tabs__menu-item" data-id="002" id="tab-2" type="button" role="tab" aria-selected="false" aria-controls="tabpanel-2" >Link 2</button>
      </div>
    </div>
    <div class="nine-tabs__items nine-g">
      <div class="nine-tabs__item nine-u-1-1 l-box -active" role="tabpanel" aria-hidden="false" id="tabpanel-1" aria-labelledby="tab-1">
        Tab copy 1
        <div class="nine-tabs__links nine-g">
          <div class="nine-tabs__link">
            <a href="#" target="_self'" class="nine-button">Link</a>
          </div>
        </div>
      </div>
      <div class="nine-tabs__item nine-u-1-1 l-box" role="tabpanel" aria-hidden="true" id="tabpanel-2" aria-labelledby="tab-2">
        Tab copy 2
        <div class="nine-tabs__links nine-g">
          <div class="nine-tabs__link">
            <a href="#" target="_self'" class="nine-button">Link</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
