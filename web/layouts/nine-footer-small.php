<footer class="nine-footer">
  <?php include $path . '/components/nine-scrolltop.php'; ?>
  <div class="nine-g">
    <div class="nine-footer__navigation navigation -footer-menu nine-u-1-1 nine-u-lg-18-24 l-box">
      <nav class="navigation -footer" role="navigation" itemscope itemtype="https://schema.org/SiteNavigationElement" aria-label="Footer navigation">
        <ul class="menu">
          <li class="menu-item"><a href="#" class="navigation__link">First Menu</a></li>
          <li class="menu-item"><a href="#" class="navigation__link">Second Menu</a></li>
          <li class="menu-item"><a href="#" class="navigation__link">Third Menu</a></li>
          <li class="menu-item"><a href="#" class="navigation__link">Fourth Menu</a></li>
        </ul>
      </nav>
    </div>
  </div>
  <div class="nine-g">
    <div class="nine-footer__company-info nine-u-1-1 nine-u-lg-16-24">
      <div class="nine-g">
        <div class="nine-footer__logo nine-u-6-24">
          <img class="nine-img" src="/assets/imgs/logo-img.png" alt="" />
        </div>
        <div class="nine-footer__name nine-u-18-24 l-box">{{ Name Of Company }}</div>
      </div>
    </div>
    <div class="nine-footer__navigation navigation -social-menu nine-u-1-1 nine-u-lg-8-24 l-box">
      <nav class="navigation -social" role="navigation" itemscope itemtype="https://schema.org/SiteNavigationElement" aria-label="Footer navigation">
        <ul class="menu">
          <li class="menu-item">
            <a href="https://www.facebook.com/AnchorBrewing" itemprop="url"><span class="visually-hidden">Facebook</span><i class="fa-brands fa-facebook"></i></a>
          </li>
          <li class="menu-item">
            <a href="https://www.instagram.com/anchorbrewing/" itemprop="url"><span class="visually-hidden">Instagram</span><i class="fa-brands fa-instagram"></i></a>
          </li>
          <li class="menu-item">
            <a href="https://twitter.com/AnchorBrewing" itemprop="url"><span class="visually-hidden">Twitter</span><i class="fa-brands fa-twitter"></i></a>
          </li>
        </ul>
      </nav>
    </div>
  </div>
  <div class="nine-g">
    <div class="nine-footer__copyright nine-u-1-1">
      <div class="l-box">
        {{ copyright goes here }}
      </div>
    </div>
  </div>
</footer>
