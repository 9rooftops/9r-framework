<?php include $path . '/components/nine-scrolltop.php'; ?>
<footer class="nine-footer nine-footer-001">
  <div class="nine-footer__intro-contact nine-g l-box">
    <div class="nine-footer__intro-menu nine-u-1-1 nine-u-lg-16-24 l-box">
      <div class="nine-footer__intro-menu-content nine-g">
        <div class="nine-footer__intro-menu-title h1 nine-u-1-1">Some Interesting links for you to check out</div>
        <div class="nine-footer__intro-menu-menu nine-u-1-1">
          <div class="nine-g">
            <div class="nine-footer__navigation navigation -footer-menu nine-u-1-1 nine-u-lg-8-24">
              <nav class="navigation -footer" role="navigation" itemscope itemtype="https://schema.org/SiteNavigationElement" aria-label="Footer navigation first">
                <ul class="menu">
                  <li class="menu-item"><a href="#" class="navigation__link -no-link">First Menu</a>
                    <ul class="sub-menu">
                      <li class="menu-item"><a href="#" class="navigation__link">Menu Item</a></li>
                      <li class="menu-item"><a href="#" class="navigation__link">Menu Item</a></li>
                      <li class="menu-item"><a href="#" class="navigation__link">Menu Item</a></li>
                    </ul>
                  </li>
                </ul>
              </nav>
            </div>
            <div class="nine-footer__navigation navigation -footer-menu nine-u-1-1 nine-u-lg-8-24">
              <nav class="navigation -footer" role="navigation" itemscope itemtype="https://schema.org/SiteNavigationElement" aria-label="Footer navigation second">
                <ul class="menu">
                  <li class="menu-item"><a href="#" class="navigation__link -no-link">Second Menu</a>
                    <ul class="sub-menu">
                      <li class="menu-item"><a href="#" class="navigation__link">Menu Item</a></li>
                      <li class="menu-item"><a href="#" class="navigation__link">Menu Item</a></li>
                      <li class="menu-item"><a href="#" class="navigation__link">Menu Item</a></li>
                    </ul>
                  </li>
                </ul>
              </nav>
            </div>
            <div class="nine-footer__navigation navigation -footer-menu nine-u-1-1 nine-u-lg-8-24">
              <nav class="navigation -footer" role="navigation" itemscope itemtype="https://schema.org/SiteNavigationElement" aria-label="Footer navigation third">
                <ul class="menu">
                  <li class="menu-item"><a href="#" class="navigation__link -no-link">Third Menu</a>
                    <ul class="sub-menu">
                      <li class="menu-item"><a href="#" class="navigation__link">Menu Item</a></li>
                      <li class="menu-item"><a href="#" class="navigation__link">Menu Item</a></li>
                      <li class="menu-item"><a href="#" class="navigation__link">Menu Item</a></li>
                    </ul>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="nine-footer__contact-us nine-u-1-1 nine-u-lg-8-24">
      <div class="nine-footer__contact-us-content nine-g">
        <div class="nine-footer__contact-us-title-menu nine-u-1-1 l-box">
          <div class="nine-footer__contact-us-title h1 nine-u-1-1">Contact Us</div>
          <div class="nine-footer__navigation navigation -footer-menu nine-u-1-1">
            <nav class="navigation -footer" role="navigation" itemscope itemtype="https://schema.org/SiteNavigationElement" aria-label="Footer navigation fourth">
              <ul class="menu">
                <li class="menu-item"><a href="#" class="navigation__link -no-link">Forth Menu</a>
                  <ul class="sub-menu">
                    <li class="menu-item"><a href="#" class="navigation__link">Menu Item</a></li>
                    <li class="menu-item"><a href="#" class="navigation__link">Menu Item</a></li>
                    <li class="menu-item"><a href="#" class="navigation__link">Menu Item</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <div class="nine-footer__navigation navigation -social-menu nine-u-1-1">
          <nav class="navigation -social" role="navigation" itemscope itemtype="https://schema.org/SiteNavigationElement" aria-label="Footer navigation social">
            <ul class="menu">
              <li class="menu-item">
                <a href="https://www.facebook.com/AnchorBrewing" itemprop="url"><span class="visually-hidden">Facebook</span><i class="fa-brands fa-facebook"></i></a>
              </li>
              <li class="menu-item">
                <a href="https://www.instagram.com/anchorbrewing/" itemprop="url"><span class="visually-hidden">Instagram</span><i class="fa-brands fa-instagram"></i></a>
              </li>
              <li class="menu-item">
                <a href="https://twitter.com/AnchorBrewing" itemprop="url"><span class="visually-hidden">Twitter</span><i class="fa-brands fa-twitter"></i></a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>
  <div class="nine-g">
    <div class="nine-footer__copyright nine-u-1-1 l-box">
      <span class="nine-footer__name">{{ Name Of Company }}</span> {{ copyright goes here }}
    </div>
  </div>
</footer>
