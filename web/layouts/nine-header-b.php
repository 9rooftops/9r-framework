<header class="nine-header">
  <div class="nine-banner">
    <div class="nine-g">
      <div class="nine-u-1-1">
        <div class="nine-banner__copy">ipsum banner copy goes here</div>
      </div>
    </div>
  </div>
  <div class="nine-header__top">
    <div class="nine-g">
      <div class="nine-header__logo nine-u-20-24 nine-u-sm-20-24 nine-u-md-20-24 nine-u-lg-20-24">
        <img src="/assets/imgs/logo-img.png" alt="logo" />
      </div>
      <div class="nine-header__search-icon nine-u-2-24 nine-u-sm-2-24 nine-u-md-2-24 nine-u-lg-4-24 -y-center -x-center">
        <a href="#"><i class="fa-regular fa-magnifying-glass"></i><span class="visually-hidden">Search</span></a>
      </div>
      <div class="nine-header__menu-icon nine-u-2-24 -y-center -x-center nine-u-sm-2-24 nine-u-md-2-24">
        <a href="#"><i class="fa-regular fa-bars"></i><span class="visually-hidden">Menu</span></a>
      </div>
      <div class="nine-header__navigation navigation -main-menu nine-u-24-24 -y-center">
        <nav itemscope itemtype="https://schema.org/SiteNavigationElement" aria-label="header navigation">
          <a href="#" class="navigation__close-icon"><i class="fa-regular fa-xmark"></i><span class="visually-hidden">Close</span></a>
          <ul class="menu">
            <li class="menu-item">First Menu
              <ul class="sub-menu">
                <li class="menu-item">
                  <a href="#" class="navigation__link">Link</a>
                </li>
                <li class="menu-item">
                  <a href="#" class="navigation__link">Link</a>
                </li>
                <li class="menu-item">
                  <a href="#" class="navigation__link">Link</a>
                </li>
              </ul>
            </li>
            <li class="menu-item">Second Menu
              <ul class="sub-menu">
                <li class="menu-item">
                  <a href="#" class="navigation__link">Link</a>
                </li>
                <li class="menu-item">
                  <a href="#" class="navigation__link">Link</a>
                </li>
                <li class="menu-item">
                  <a href="#" class="navigation__link">Link</a>
                </li>
              </ul>
            </li>
            <li class="menu-item">Third Menu
              <ul class="sub-menu">
                <li class="menu-item">
                  <a href="#" class="navigation__link">Link</a>
                </li>
                <li class="menu-item">
                  <a href="#" class="navigation__link">Link</a>
                </li>
                <li class="menu-item">
                  <a href="#" class="navigation__link">Link</a>
                </li>
              </ul>
            </li>
            <li class="menu-item">Forth Menu
              <ul class="sub-menu">
                <li class="menu-item">
                  <a href="#" class="navigation__link">Link</a>
                </li>
                <li class="menu-item">
                  <a href="#" class="navigation__link">Link</a>
                </li>
                <li class="menu-item">
                  <a href="#" class="navigation__link">Link</a>
                </li>
              </ul>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</header>
