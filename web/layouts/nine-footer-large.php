<footer class="nine-footer">
  <?php include $path . '/components/nine-scrolltop.php'; ?>
  <div class="nine-g">
    <div class="nine-footer__navigation navigation -footer-menu nine-u-1-1 nine-u-lg-18-24 l-box">
      <nav class="navigation" role="navigation" itemscope itemtype="https://schema.org/SiteNavigationElement" aria-label="Footer navigation">
        <ul class="menu">
          <li class="menu-item">First Menu
            <ul class="sub-menu">
              <li class="menu-item">
                <a href="#" class="navigation__link">Link</a>
              </li>
              <li class="menu-item">
                <a href="#" class="navigation__link">Link</a>
              </li>
              <li class="menu-item">
                <a href="#" class="navigation__link">Link</a>
              </li>
            </ul>
          </li>
          <li class="menu-item">Second Menu
            <ul class="sub-menu">
              <li class="menu-item">
                <a href="#" class="navigation__link">Link</a>
              </li>
              <li class="menu-item">
                <a href="#" class="navigation__link">Link</a>
              </li>
              <li class="menu-item">
                <a href="#" class="navigation__link">Link</a>
              </li>
            </ul>
          </li>
          <li class="menu-item">Third Menu
            <ul class="sub-menu">
              <li class="menu-item">
                <a href="#" class="navigation__link">Link</a>
              </li>
              <li class="menu-item">
                <a href="#" class="navigation__link">Link</a>
              </li>
              <li class="menu-item">
                <a href="#" class="navigation__link">Link</a>
              </li>
            </ul>
          </li>
          <li class="menu-item">Forth Menu
            <ul class="sub-menu">
              <li class="menu-item">
                <a href="#" class="navigation__link">Link</a>
              </li>
              <li class="menu-item">
                <a href="#" class="navigation__link">Link</a>
              </li>
              <li class="menu-item">
                <a href="#" class="navigation__link">Link</a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
    </div>
    <div class="nine-footer__newsletter nine-u-1-1 nine-u-lg-6-24 l-box">
      <div class="newsletter-signup">
        <div class="newsletter-signup__header">Sign up</div>
        <form class="newsletter-signup__form">
          <label for="email" id="">Your email address</label>
          <input id="email" name="email" type="email" />
          <button class="nine-button" type="submit">Sign up</button>
        </form>
      </div>
    </div>
  </div>
  <div class="nine-g">
    <div class="nine-footer__company-info nine-u-1-1 nine-u-lg-16-24 l-box">
      <div class="nine-g">
        <div class="nine-footer__logo nine-u-6-24">
          <img class="nine-img" src="/assets/imgs/logo-img.png" alt="" />
        </div>
        <div class="nine-footer__name nine-u-18-24 l-box">{{ Name Of Company }}</div>
        <div class="nine-footer__contact nine-u-1-1 l-box">
          <div class="nine-footer__address nine-u-1-1">
            <address>{{ Company Address }}</address>
          </div>
          <div class="nine-footer__phone nine-u-1-1">
            <a href="tel:555-555-5555" title="Call 555-555-5555">555-555-5555</a>
          </div>
          <div class="nine-footer__email nine-u-1-1">
            <a href="mailto:info@company.com" title="Send email to info@company.com">info@company.com</a>
          </div>
        </div>
      </div>
    </div>
    <div class="nine-footer__navigation navigation -social-menu nine-u-1-1 nine-u-lg-8-24 l-box">
      <nav class="navigation" role="navigation" itemscope itemtype="https://schema.org/SiteNavigationElement" aria-label="Footer navigation">
        <ul class="menu">
          <li class="menu-item">
            <a href="https://www.facebook.com/" itemprop="url"><span class="visually-hidden">Facebook</span><i class="fa-brands fa-facebook"></i></a>
          </li>
          <li class="menu-item">
            <a href="https://www.instagram.com/" itemprop="url"><span class="visually-hidden">Instagram</span><i class="fa-brands fa-instagram"></i></a>
          </li>
          <li class="menu-item">
            <a href="https://twitter.com/" itemprop="url"><span class="visually-hidden">Twitter</span><i class="fa-brands fa-twitter"></i></a>
          </li>
        </ul>
      </nav>
    </div>
  </div>
  <div class="nine-g">
    <div class="nine-footer__copyright nine-u-1-1">
      <div class="l-box">
        {{ copyright goes here }}
      </div>
    </div>
  </div>
</footer>
