<header class="nine-header">
  <div class="nine-banner">
    <div class="nine-g">
      <div class="nine-u-1-1">
        <div class="nine-banner__copy">ipsum banner copy goes here</div>
      </div>
    </div>
  </div>
  <div class="nine-header__top">
    <div class="nine-g">
      <div class="nine-header__logo nine-u-20-24 nine-u-sm-20-24 nine-u-md-20-24 nine-u-lg-2-24">
        <img src="/assets/imgs/logo-img.png" alt="logo" />
      </div>
      <div class="nine-header__navigation navigation -main-menu nine-u-1-1 nine-u-sm-1-1 nine-u-md-1-1 nine-u-lg-18-24 -y-center">
        <nav itemscope itemtype="https://schema.org/SiteNavigationElement" aria-label="header navigation">
          <a href="#" class="navigation__close-icon"><i class="fa-regular fa-xmark"></i><span class="visually-hidden">Close</span></a>
          <ul class="menu">
            <li class="menu-item menu-item-has-children">
              <a href="#">First Menu</a>
              <ul class="sub-menu">
                <li class="menu-item">
                  <a href="#" class="navigation__link">Link</a>
                </li>
                <li class="menu-item">
                  <a href="#" class="navigation__link">Link</a>
                </li>
                <li class="menu-item">
                  <a href="#" class="navigation__link">Link</a>
                </li>
              </ul>
            </li>
            <li class="menu-item menu-item-has-children">
              <a href="#">Second Menu</a>
              <ul class="sub-menu">
                <li class="menu-item">
                  <a href="#" class="navigation__link">Link</a>
                </li>
                <li class="menu-item">
                  <a href="#" class="navigation__link">Link</a>
                </li>
                <li class="menu-item">
                  <a href="#" class="navigation__link">Link</a>
                </li>
              </ul>
            </li>
            <li class="menu-item menu-item-has-children">
              <a href="#">Third Menu</a>
              <ul class="sub-menu">
                <li class="menu-item">
                  <a href="#" class="navigation__link">Link</a>
                </li>
                <li class="menu-item">
                  <a href="#" class="navigation__link">Link</a>
                </li>
                <li class="menu-item">
                  <a href="#" class="navigation__link">Link</a>
                </li>
              </ul>
            </li>
            <li class="menu-item menu-item-has-children">
              <a href="#">Fourth Menu</a>
              <ul class="sub-menu">
                <li class="menu-item">
                  <a href="#" class="navigation__link">Link</a>
                </li>
                <li class="menu-item">
                  <a href="#" class="navigation__link">Link</a>
                </li>
                <li class="menu-item">
                  <a href="#" class="navigation__link">Link</a>
                </li>
              </ul>
            </li>
          </ul>
        </nav>
      </div>
      <div class="nine-header__search-icon nine-u-2-24 nine-u-sm-2-24 nine-u-md-2-24 nine-u-lg-4-24 -y-center -x-center">
        <a href="#"><i class="fa-regular fa-magnifying-glass"></i><span class="visually-hidden">Search</span></a>
      </div>
      <div class="nine-header__menu-icon nine-u-2-24 nine-u-sm-2-24 nine-u-md-2-24 -y-center -x-center">
        <a href="#"><i class="fa-regular fa-bars"></i><span class="visually-hidden">Menu</span></a>
      </div>
    </div>
  </div>
</header>
