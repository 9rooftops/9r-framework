<?php
// Page attributes.
$page_title = 'Nine Component Library';
$path = $_SERVER['DOCUMENT_ROOT'];
include $path . '/includes/head.php';
?>

  <body>
    <?php include $path . '/includes/header.php'; ?>
    <main>
      <div class="nine-section -full">
        <div class="nine-section__content -contained">
          <div class="nine-section__items nine-g">
            <div id="content" class="nine-section__item nine-u-1-1 l-box">
              <h1>Structure</h1>


<p>The SCSS framework is broken down based on an “Atomic” schema and uses the following folder structure:</p>

<h2>Components</h2>
<ul>
  <li>/source</li>
  <li>/scss</li>
  <li>/components</li>
</ul>

<p>Components can be pulled into the primary style.scss file or, if they are to be scoped only to their respective component, they can pull in any other global style as needed.</p>

<p>UnCSS can be used to strip out unused css if the style is to only be component based.</p>

<p>Make sure to test your templates against your UnCSS’d file as modifiers that are added via JavaScript will need to be manually added as they are not located in your template and as such will not be included in your compressed file.</p>

<h2>Elements</h2>
<ul>
  <li>/source</li>
  <li>/scss</li>
  <li>/elements</li>
</ul>

<p>Elements are again broken into an atomic structure. Each element scss file contains defaults that can styless for the elements that belong to that group.</p>

<strong>Example:</strong>

<p>The _forms.scss file will contain all elements that are used when building a standard form such as input and select fields.</p>

<p>Most elements utilize the following files:</p>
<ul>
  <li>/source</li>
  <li>/scss</li>
  <li>partials</li>
  <li>_grid.scss</li>
  <li>_variables.scss</li>
</ul>

<h2>Fontawesome</h2>
<p>The font awesome fonts are packages with our framework by default. If these are unused they should be remarked out in the master style.scss file.</p>

<p>Remember to update the fontawesome _variables.scss file to set the proper font path.</p>

<h2>Layouts</h2>
<p>The layout folder contains scss files for specific pages. This allows for an easy and clean way to set proper page specific setting that are scoped accordingly.</p>

<h2>Partials</h2>
<p>The partials folder contains default files such as global variables, mixin as well as our grid system.</p>

<p>The _variables.scss file should be set to match the style guide. This will help in setting global defaults.</P>

<h2>Regions</h2>
<p>Regions are areas designated on a page for a specific purpose. This would include things such as the header, information area, left side, main content, right side, footer, etc.</p>

<h2>Vendors</h2>
<p>The vendors folder is used for any 3 party plugins that may be needed such as an image slider.</p>
            </div>
          </div>
        </div>
      </div>
    </main>

<?php include $path . '/includes/footer.php'; ?>


