axe.run({
  runOnly: {
    type: 'tag',
    values: ['wcag2a', 'wcag2aa', 'ACT', 'cat.aria', 'cat.color', 'cat.forms', 'cat.keyboard', 'cat.name-role-value', 'cat.parsing', 'cat.semantics', 'cat.tables', 'cat.text-alternatives']
  }
},
(err, results) => {
  console.log(results);
  console.log(err);
  nine_axe_results(results);
});
function nine_axe_results(results) {
  jQuery('.nine-axe__passes .count').text('Passed: ' + results.passes.length);
  jQuery('.nine-axe__violations .count').text('Violations: ' + results.violations.length);
  nine_axe_violations(results);
}
function nine_axe_violations(results) {
  const incomplete = results.incomplete;
  const violations = results.violations;

  jQuery('.nine-axe .nine-axe__violations .items').append('<div class="h3">Incomplete</div>');
  incomplete.forEach(item => {
    if (item.impact === 'minor') {
      alert_type = '-info';
    } else if (item.impact === 'moderate') {
      alert_type = '-warning';
    } else if (item.impact === 'critical' || item.impact === 'serious') {
      alert_type = '-error';
    } else {
      alert_type = '';
    }
    var nine_alert_a = '<div class="nine-alert ' + alert_type + '" role="alert"><div class="nine-alert__content nine-g"><div class="nine-u-1-1"><div class="l-box"><div class="nine-alert__heading h2">' + item.impact + '</div><div class="nine-alert__text">';
    var nine_alert_b = '</div></div></div></div></div>';
    jQuery('.nine-axe .nine-axe__violations .items').append(nine_alert_a + nine_alert_b);

    var impact = '<div><strong>Impact:</strong> ' + item.impact + '</div>';
    var description = '<div><strong>Description:</strong> ' + item.description.replace('<', '&lt;').replace('>', '&gt;') + '</div>';
    var help = '<div><strong>Help:</strong> ' + item.help + '</div>';
    var help_url = '<div> <a href="' + item.helpUrl + '" target="_blank">Ext. Reference</a></div>';

    item.nodes.forEach(node => {
      var failureSummary = '<div><strong>Failure Summary:</strong> ' + node.failureSummary + '</div>';
      var target = '<div><strong>Target Element:</strong> ' + node.target[0] + '</div>';
      jQuery('.nine-axe .nine-axe__violations .items').append(failureSummary + target);
    });
    jQuery('.nine-axe .nine-axe__violations .items').append(description + help + help_url + '<hr />');
  });

  jQuery('.nine-axe .nine-axe__violations .items').append('<div class="h3">Violations</div>');
  violations.forEach(item => {
    if (item.impact === 'moderate') {
      alert_type = '-warning';
    } else if (item.impact === 'critical' || item.impact === 'serious') {
      alert_type = '-error';
    } else {
      alert_type = '';
    }
    var nine_alert_a = '<div class="nine-alert ' + alert_type + '" role="alert"><div class="nine-alert__content nine-g"><div class="nine-u-1-1"><div class="l-box"><div class="nine-alert__heading h2">' + item.impact + '</div><div class="nine-alert__text">';
    var nine_alert_b = '</div></div></div></div></div>';
    jQuery('.nine-axe .nine-axe__violations .items').append(nine_alert_a + nine_alert_b);

    var impact = '<div><strong>Impact:</strong> ' + item.impact + '</div>';
    var description = '<div><strong>Description:</strong> ' + item.description.replace('<', '&lt;').replace('>', '&gt;') + '</div>';
    var help = '<div><strong>Help:</strong> ' + item.help + '</div>';
    var help_url = '<div> <a href="' + item.helpUrl + '" target="_blank">Ext. Reference</a></div>';

    item.nodes.forEach(node => {
      var failureSummary = '<div><strong>Failure Summary:</strong> ' + node.failureSummary + '</div>';
      var target = '<div><strong>Target Element:</strong> ' + node.target[0] + '</div>';
      jQuery('.nine-axe .nine-axe__violations .items').append(failureSummary + target);
    });
    jQuery('.nine-axe .nine-axe__violations .items').append(description + help + help_url + '<hr />');
  });
}

jQuery(window).on('load', function () {
  jQuery('.nine-axe').toggleClass('-close');
  jQuery('.nine-axe').on('click', '.nine-axe__open-close', function () {
    jQuery('.nine-axe').toggleClass('-close');
  })
});
