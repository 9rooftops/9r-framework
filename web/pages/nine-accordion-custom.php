<?php
// Page attributes.
$page_title = 'Nine Accordion';
$path = $_SERVER['DOCUMENT_ROOT'];
include $path . '/includes/head.php';
?>

<body>
  <?php include $path . '/includes/header.php'; ?>
  <main>
    <h1>Accordion -Custom</h1>
    <div class="nine-section -contained">
      <div class="nine-section__content">
        <div class="nine-section__body">
          <p>Custom code (use if IE 11 is required)</p>
        </div>
        <div class="nine-section__items nine-g">
          <div class="nine-section__item nine-u-1-1 l-box">
            <?php include $path . '/components/nine-accordion-custom.php'; ?>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          </div>
        </div>
      </div>
    </div>
  </main>
</body>

<?php include $path . '/includes/footer.php'; ?>
