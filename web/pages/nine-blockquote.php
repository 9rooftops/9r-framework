<?php
// Page attributes.
$page_title = 'Nine blockquote';
$path = $_SERVER['DOCUMENT_ROOT'];
include $path . '/includes/head.php';
?>

<body>
  <?php include $path . '/includes/header.php'; ?>
  <main>
    <h1>Blockquote</h1>
    <div class="nine-section -full">
      <div class="nine-section__content -full">
        <div class="nine-section__items nine-g">
          <div class="nine-section__item nine-u-1-1">
            <?php include $path . '/components/nine-blockquote.php'; ?>
          </div>
        </div>
      </div>
    </div>
  </main>
</body>

<?php include $path . '/includes/footer.php'; ?>
