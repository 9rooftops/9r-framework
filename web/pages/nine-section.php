<?php
// Page attributes.
$page_title = 'Nine section';
$path = $_SERVER['DOCUMENT_ROOT'];
include $path . '/includes/head.php';
?>

  <body>
    <?php include $path . '/includes/header.php'; ?>
    <main>
      <h1>Section</h1>
      <?php include $path . '/components/nine-section.php'; ?>

    </main>
  </body>

<?php include $path . '/includes/footer.php'; ?>
