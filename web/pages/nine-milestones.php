<?php
// Page attributes.
$page_title = 'Nine milestones';
$path = $_SERVER['DOCUMENT_ROOT'];
include $path . '/includes/head.php';
?>

<body>
  <?php include $path . '/includes/header.php'; ?>
  <main>
    <h1>Milestones</h1>
    <div class="nine-section">
      <div class="nine-section__content">
        <div class="nine-section__items nine-g">
          <div class="nine-section__item nine-u-1-1">
            <?php include $path . '/components/nine-milestones.php'; ?>
          </div>
        </div>
      </div>
    </div>
  </main>
</body>

<?php include $path . '/includes/footer.php'; ?>
