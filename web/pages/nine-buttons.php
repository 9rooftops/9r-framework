<?php
// Page attributes.
$page_title = 'Nine buttons';
$path = $_SERVER['DOCUMENT_ROOT'];
include $path . '/includes/head.php';
?>

<body>
  <?php include $path . '/includes/header.php'; ?>
  <main>
    <h1>Buttons</h1>
    <div class="nine-section -full">
      <div class="nine-section__content -contained">
        <div class="nine-section__items nine-g">
          <div class="nine-section__item nine-u-1-1 l-box">
            <?php include $path . '/elements/nine-buttons.php'; ?>
          </div>
        </div>
      </div>
    </div>
  </main>
</body>

<?php include $path . '/includes/footer.php'; ?>
