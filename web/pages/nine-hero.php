<?php
// Page attributes.
$page_title = 'Nine Hero';
$path = $_SERVER['DOCUMENT_ROOT'];
include $path . '/includes/head.php';
?>

  <body>
    <?php include $path . '/includes/header.php'; ?>
    <main>
      <div class="nine-section -full">
        <div class="nine-section__content -full">
          <div class="nine-section__items nine-g">
            <div class="nine-section__item nine-u-1-1">
              <?php include $path . '/components/nine-hero.php'; ?>
            </div>
          </div>
        </div>
      </div>
    </main>
  </body>

<?php include $path . '/includes/footer.php'; ?>
