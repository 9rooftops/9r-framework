<?php
// Page attributes.
$page_title = 'Nine Footer 001';
$path = $_SERVER['DOCUMENT_ROOT'];
include $path . '/includes/head.php';
?>

<body>
  <?php include $path . '/includes/header.php'; ?>
  <main>
    <h1>Footer 001</h1>
    <div class="nine-section">
      <div class="nine-section__content">
        <div class="nine-section__items nine-g">
          <div class="nine-section__item nine-u-1-1">
            <?php include $path . '/layouts/nine-footer-001.php'; ?>
          </div>
        </div>
      </div>
    </div>
  </main>
</body>

<?php include $path . '/includes/footer.php'; ?>
