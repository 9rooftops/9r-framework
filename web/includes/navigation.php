<ul class="menu -no-style" role="menubar">
  <li class="menu__item" role="none">
    <a href="/" class="menu__link" role="menuitem" tabindex="0">Home</a>
  </li>
  <li class="menu-item menu-item-has-children" role="none" aria-expanded="false">
    <a href="#" class="menu__link" role="menuitem" aria-expanded="false">Components</a>
    <ul class="sub-menu -no-style" role="menu">
      <li class="menu__item" role="none">
        <a href="/pages/nine-accordion.php" class="menu__link" role="menuitem" tabindex="-1">Nine Accordion</a>
      </li>
      <li class="menu__item" role="none">
        <a href="/pages/nine-accordion-custom.php" class="menu__link" role="menuitem" tabindex="-1">Nine Accordion -Custom</a>
      </li>
      <li class="menu__item" role="none">
        <a href="/pages/nine-alternating.php" hx-on::after-request="nine_is_visible()" class="menu__link" role="menuitem" tabindex="-1">Nine alternating</a>
      </li>
      <li class="menu__item" role="none">
        <a href="/pages/nine-alert.php" class="menu__link" role="menuitem" tabindex="-1">Nine Alert</a>
      </li>
      <li class="menu__item" role="none">
        <a href="/pages/nine-alert-error.php" class="menu__link" role="menuitem" tabindex="-1">Nine Alert -Error</a>
      </li>
      <li class="menu__item" role="none">
        <a href="/pages/nine-alert-info.php" class="menu__link" role="menuitem" tabindex="-1">Nine Alert -Info</a>
      </li>
      <li class="menu__item" role="none">
        <a href="/pages/nine-alert-success.php" class="menu__link" role="menuitem" tabindex="-1">Nine Alert -Success</a>
      </li>
      <li class="menu__item" role="none">
        <a href="/pages/nine-alert-warning.php" class="menu__link" role="menuitem" tabindex="-1">Nine Alert -Warning</a>
      </li>
      <li class="menu__item" role="none">
        <a href="/pages/nine-banner.php" class="menu__link" role="menuitem" tabindex="-1">Nine Banner</a>
      </li>
      <li class="menu__item" role="none">
        <a href="/pages/nine-card.php" class="menu__link" role="menuitem" tabindex="-1">Nine Card</a>
      </li>
      <li class="menu__item" role="none">
        <a href="/pages/nine-card-reveal.php" class="menu__link" role="menuitem" tabindex="-1">Nine Card -Reveal</a>
      </li>
      <li class="menu__item" role="none">
        <a href="/pages/nine-cta.php" class="menu__link" role="menuitem" tabindex="-1">Nine cta</a>
      </li>
      <li class="menu__item" role="none">
        <a href="/pages/nine-hero.php" class="menu__link" role="menuitem" tabindex="-1">Nine hero</a>
      </li>
      <li class="menu__item" role="none">
        <a href="/pages/nine-section.php" class="menu__link" role="menuitem" tabindex="-1">Nine Section</a>
      </li>
      <li class="menu__item" role="none">
        <a href="/pages/nine-tabs.php" class="menu__link" role="menuitem" tabindex="-1">Nine Tabs</a>
      </li>
      <li class="menu__item" role="none">
        <a href="/pages/nine-milestones.php" class="menu__link" role="menuitem" tabindex="-1">Nine milestones</a>
      </li>
      <li class="menu__item" role="none">
        <a href="/pages/nine-blockquote.php" class="menu__link" role="menuitem" tabindex="-1">Nine blockquote</a>
      </li>
    </ul>
  </li>
  <li class="menu__item menu-item-has-children" role="none" aria-expanded="false">
    <a href="#" class="menu__link" role="menuitem" aria-expanded="false">Layouts</a>
    <ul class="sub-menu -no-style" role="menu">
      <li class="menu__item" role="none">
        <a href="/pages/nine-footer-large.php" class="menu__link" role="menuitem" tabindex="-1">Nine Footer Large</a>
      </li>
      <li class="menu__item" role="none">
        <a href="/pages/nine-footer-medium.php" class="menu__link" role="menuitem" tabindex="-1">Nine Footer Medium</a>
      </li>
      <li class="menu__item" role="none">
        <a href="/pages/nine-footer-small.php" class="menu__link" role="menuitem" tabindex="-1">Nine Footer Small</a>
      </li>
      <li class="menu__item" role="none">
        <a href="/pages/nine-footer-001.php" class="menu__link" role="menuitem" tabindex="-1">Nine Footer 001</a>
      </li>
      <li class="menu__item" role="none">
        <a href="/pages/nine-header-a.php" class="menu__link" role="menuitem" tabindex="-1">Nine Header A</a>
      </li>
      <li class="menu__item" role="none">
        <a href="/pages/nine-header-b.php" class="menu__link" role="menuitem" tabindex="-1">Nine Header B</a>
      </li>
      <li class="menu__item" role="none">
        <a href="/pages/nine-grid.php" class="menu__link" role="menuitem" tabindex="-1">Nine Grid</a>
      </li>
    </ul>
  </li>
  <li class="menu__item menu-item-has-children" role="none" aria-expanded="false">
    <a href="#" class="menu__link" role="menuitem" aria-expanded="false">Elements</a>
    <ul class="sub-menu -no-style" role="menu">
      <li class="menu__item" role="none">
        <a href="/pages/nine-buttons.php" class="menu__link" role="menuitem" tabindex="-1">Nine Buttons</a>
      </li>
      <li class="menu__item" role="none">
        <a href="/pages/nine-forms.php" class="menu__link" role="menuitem" tabindex="-1">Nine Forms</a>
      </li>
    </ul>
  </li>
</ul>
