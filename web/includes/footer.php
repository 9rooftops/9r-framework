    <div class="nine-axe" role="complementary">
      <div class="nine-axe__open-close -open nine-g -x-center -y-center"><i class="fa-solid fa-chevrons-right"></i></div>
      <div class="nine-axe__content">
        <div class="h2">Axe compliance results</div>
        <div class="nine-axe__passes">
          <div class="count"></div>
        </div>
        <div class="nine-axe__violations">
          <div class="count"></div>
          <div class="items"></div>
        </div>
      </div>
    </div>
    <?php include $path . '/components/nine-scrolltop.php'; ?>
    <footer class="nine-footer">
      <div class="nine-g">
        <div class="nine-footer__company-info nine-u-1-1 nine-u-lg-16-24 l-box">
          <div class="nine-g">
            <div class="nine-footer__logo nine-u-6-24">
              <img class="nine-img" src="/assets/imgs/logo-img.png" alt="Nine logo" />
            </div>
            <div class="nine-footer__name nine-u-18-24 l-box">9Rooftops</div>
            <div class="nine-footer__contact nine-u-1-1 l-box">
              <div class="nine-footer__address nine-u-1-1">
                <address>6 Anolyn Ct. Bluffton, South Carolina 29910</address>
              </div>
              <div class="nine-footer__phone nine-u-1-1">
                <a href="tel:555-555-5555" title="Call 555-555-5555">555-555-5555</a>
              </div>
              <div class="nine-footer__email nine-u-1-1">
                <a href="mailto:info@company.com" title="Send email to info@company.com">info@9rooftops.com</a>
              </div>
            </div>
          </div>
        </div>
        <div class="nine-footer__navigation navigation -social-menu nine-u-1-1 nine-u-lg-8-24 l-box">
          <nav class="navigation" role="navigation" itemscope itemtype="https://schema.org/SiteNavigationElement" aria-label="Footer navigation">
            <ul class="menu">
              <li class="menu-item">
                <a href="https://www.facebook.com/9rooftops" itemprop="url"><span class="visually-hidden">Facebook</span><i class="fa-brands fa-facebook"></i></a>
              </li>
              <li class="menu-item">
                <a href="https://www.instagram.com/9rooftops/" itemprop="url"><span class="visually-hidden">Instagram</span><i class="fa-brands fa-instagram"></i></a>
              </li>
              <li class="menu-item">
                <a href="https://www.linkedin.com/company/9rooftops/mycompany/" itemprop="url"><span class="visually-hidden">Linkedin</span><i class="fa-brands fa-linkedin"></i></a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
      <div class="nine-g">
        <div class="nine-footer__copyright nine-u-1-1">
          <div class="l-box">
            © <?php echo date("Y"); ?> 9Rooftops Marketing, LLC
          </div>
        </div>
      </div>
    </footer>

    <script src="/assets/vendors/axe/axe.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous" defer></script>
    <script src="/assets/js/main.js" defer></script>
    <script src="/assets/vendors/axe/nine-axe.js" defer></script>

    <script>


    </script>
  </body>
</html>
