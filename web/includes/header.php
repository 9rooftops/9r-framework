<header class="nine-header">
  <div class="nine-header__top">
    <div class="nine-g">
      <div class="nine-header__logo nine-u-20-24 nine-u-sm-20-24 nine-u-md-20-24 nine-u-lg-2-24">
        <img src="/assets/imgs/logo-img.png" alt="logo" />
      </div>
      <div class="nine-header__navigation navigation -main-menu nine-u-1-1 nine-u-sm-1-1 nine-u-md-1-1 nine-u-lg-18-24 -y-center">
        <nav itemscope itemtype="https://schema.org/SiteNavigationElement" aria-label="header navigation">
          <?php include $path . '/includes/navigation.php' ?>
        </nav>
      </div>
      <div class="nine-header__search-icon nine-u-2-24 nine-u-sm-2-24 nine-u-md-2-24 nine-u-lg-4-24 -y-center -x-center">
        <a href="#" class="nine-search__toggle fa-regular fa-magnifying-glass -hidden"><span class="visually-hidden">Search</span></a>
      </div>
      <div class="nine-header__menu-icon nine-u-2-24 nine-u-sm-2-24 nine-u-md-2-24 -y-center -x-center">
        <a href="#" class="nine-menu__toggle fa-regular fa-bars"><span class="visually-hidden">Menu</span></a>
      </div>
    </div>
  </div>
</header>
