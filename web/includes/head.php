<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
    <meta name="description" content="">
    <meta name="author" content="9Rooftops">
    <title>9R-Framework | <?php if (isset($page_title)) : echo $page_title; else : echo 'Home'; endif; ?></title>
    <link rel="stylesheet" href="/assets/css/style.min.css">
  </head>
