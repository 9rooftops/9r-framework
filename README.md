<a href="https://9rt-app-prd-framework-eus.azurewebsites.net" target="_blank"><img src="https://i.imgur.com/QU0Kg4K.png" width="75px" /></a>
# 9Rooftops Framework

### Project Description
This project is an ongoing WIP for a full framework that can be used on all
9Rooftops websites. This framework should include simple styling to allow for
full creative control.

### Setup local development
The following steps will help in setting up the local environment for this
project.
```bash
$ cd ~/projects/
$ git clone git@bitbucket.org:9rooftops/9r-framework.git ./9r-framework
$ cd ~/projects/9r-framework
```
tart your local env.
```bash
lando rebuild -y
```

## Local dev tooling commands
```bash
lando grunt build
lando grunt dev
lando grunt favicon
lando grunt sync
```
`lando grunt dev` will build, watch and sync.

----

## Contributing
For major changes, create a branch that you will work in. Once fully tested,
the branch can be merged into master.

## Updating NPM package
Once updates have been approved and merged into master, they can be merged
into the npm-package branch. This branch has a few minor edits to allow for
projects to pull in and override where needed. Be sure to update the following file
were needed.
### Increment the version number.
```bash
package.json
```
### Add new components
```bash
/source/scss/9r-framework.scss
```
*You will need to add any newly created components to this file so they will be
part of the build process.*


You can then submit the package via the following command.
```bash
lando npm publish --access public
```

----

### Change Log
*01152025*
* Added new version of fontawesome

*08012024*
* Added [AOS](https://michalsnik.github.io/aos/) for animations
* Removed dark theme so it can be added only when needed

----

## Software Requirements
- [Docker](https://docs.docker.com/get-docker/)
- [Lando](https://github.com/lando/lando/releases)
- [GIT](https://git-scm.com/downloads)
- [GitKraken](https://www.gitkraken.com/download) -optional


## License
- [MIT](https://choosealicense.com/licenses/mit/)
