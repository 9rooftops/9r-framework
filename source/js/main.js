// JavaScript Document
jQuery.noConflict();

/**
* set globals
**/
window_width = jQuery(window).width();



/**
 * set height of all postEmbed to be equal
 */
function equalHeights(element){
  jQuery(element).css('height','auto');
  rowHeight = -1;
  jQuery(element).each(function() {
    rowHeight = rowHeight > jQuery(this).height() ? rowHeight : jQuery(this).height();
  });
  window_width = jQuery(window).width();
  if( window_width >= 708 ){
    jQuery(element).height(rowHeight);
  }
  else{
    jQuery(element).height('auto');
  }
}
function runEqualHeights(){
  setTimeout(function(){
    //equalHeights('.articleLayout .articleLayout__item');
  },1200);
}




/**
* all document ready functions
**/
jQuery(document).ready(function($){
  window_width = $(window).width();
  header_height = $('#header').height();

/**
* trigger for window resize
**/
  $(window).resize(function() {
    if(this.resizeTO) clearTimeout(this.resizeTO);
    this.resizeTO = setTimeout(function() {
      $(this).trigger('resizeEnd');
    }, 500);
  });
  $(window).bind('resizeEnd', function() {
    window_width = $(window).width();
    // set postImportLayout__item height
    // also in window.log and ajaxComplete
    runEqualHeights();
  });


});
// Close document ready





/**
* all window load function
* only for items that need to wait for the all elements to finish loading.
**/
jQuery(window).on('load', function () {

  runEqualHeights();

  AOS.init({
    // Global settings:
    disable: false, // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
    startEvent: 'DOMContentLoaded', // name of the event dispatched on the document, that AOS should initialize on
    initClassName: 'aos-init', // class applied after initialization
    animatedClassName: 'aos-animate', // class applied on animation
    useClassNames: false, // if true, will add content of `data-aos` as classes on scroll
    disableMutationObserver: false, // disables automatic mutations' detections (advanced)
    debounceDelay: 50, // the delay on debounce used while resizing window (advanced)
    throttleDelay: 99, // the delay on throttle used while scrolling the page (advanced)
    // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
    offset: 120, // offset (in px) from the original trigger point
    delay: 0, // values from 0 to 3000, with step 50ms
    duration: 400, // values from 0 to 3000, with step 50ms
    easing: 'ease', // default easing for AOS animations
    once: false, // whether animation should happen only once - while scrolling down
    mirror: false, // whether elements should animate out while scrolling past them
    anchorPlacement: 'top-bottom', // defines which position of the element regarding to window should trigger the animation
  });
});




/**
* all ajax complete functions
**/
jQuery(document).ajaxComplete(function(event, request, setting){
  // set postImportLayout__item height
    // also in window.log and ajaxComplete
    runEqualHeights();
});
