function nine_animate() {
  jQuery(window).on('load', function() {
    /**
    * trigger for window scroll and any functions that need to run based on scroll
    * depth or visibility during scroll.
    **/
     jQuery(window).scroll(function () {
      if (this.scrollTO) clearTimeout(this.scrollTO);
      this.scrollTO = setTimeout(function () {
        jQuery(this).trigger('scrollEnd');
      }, 0);
    });
    jQuery(window).bind('scrollEnd', function () {
      // Check for visibility to set a proper animation
      jQuery('.-animate').each(function () {
        if (jQuery(this).visible(true)) {
          jQuery(this).addClass('-is-visible');
        }
      });
    });
  });
}
nine_animate();
