function nine_search() {
  jQuery(window).on('load', function () {
    // Open close search
    jQuery('body').on('click', '.nine-search__toggle', function (event) {
      event.preventDefault();
      jQuery(this).toggleClass('fa-magnifying-glass fa-times');
      jQuery('.nine-header__search').toggleClass('-active');
      jQuery('.nine-search .form-search').focus();
      // close menu if open
      nine_menu_close();
    });
    // Escape key to close search
    jQuery('body').on('keydown', function (event) {
      if (event.which == '27' && jQuery('.nine-header__search').hasClass('-active')) {
        nine_search_close();
      }
    });
  });
}
// Used to force the search to close.
// Can also be used from other items like menu
function nine_search_close() {
  if (jQuery('.nine-header__search').hasClass('-active')) {
    jQuery('.nine-header__search').removeClass('-active');
    jQuery('.nine-search__toggle').toggleClass('fa-magnifying-glass fa-times');
  }
}
nine_search();
