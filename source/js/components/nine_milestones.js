// This function is called from the nine_animate.js file
function nine_milestones_count() {
  jQuery(window).on('load', function () {
    jQuery('.nine-milestones .nine-milestones__percentage').each(function () {
      var milestone_start = 0;
      var milestone_end = jQuery(this).data('number');
      var milestone_elem = jQuery(this).children('.number');
      var milestone_animate = setInterval(function () {
        if (milestone_start < (milestone_end + 1)) {
          milestone_elem.text(milestone_start);
          milestone_start++;
        }
        else {
          clearInterval(milestone_animate);
        }
      }, 60);
    });
  });
}
nine_milestones_count();
