function nine_tabs() {
  jQuery(window).on('load', function () {
    jQuery(".nine-tabs").on("click", ".nine-tabs__menu-open", function () {
      jQuery(this).parent().parent().toggleClass("-active");
    });
    jQuery(".nine-tabs").on("click", ".nine-tabs__menu-item", function () {
      // The data-id is used to scope each group of tabs
      var nine_tab_component = jQuery(this).data('id');
      var aria_selected = jQuery(this).attr('aria-selected');
      var aria_controls = jQuery(this).attr('aria-controls');
      var nine_tab_txt = jQuery(this).text();
      if (aria_selected === 'true') {
        // Toggle class and aria attrs
        jQuery('.nine-tabs[data-id="' + nine_tab_component + '"]').find('.nine-tabs__placeholder').text('- Make Selection -');
        jQuery(this).removeClass('-active').attr('aria-selected', 'false');
        jQuery('.nine-tabs[data-id="' + nine_tab_component + '"]').find('#' + aria_controls).removeClass('-active').attr('aria-hidden','true');
      }
      else {
        // Close all tabs and reset classes and aria attrs
        jQuery('.nine-tabs[data-id="' + nine_tab_component + '"]').find('.nine-tabs__menu-item, .nine-tabs__item').removeClass('-active');
        jQuery('.nine-tabs[data-id="' + nine_tab_component + '"]').find('.nine-tabs__menu-item').attr('aria-selected', 'false');
        jQuery('.nine-tabs[data-id="' + nine_tab_component + '"]').find('.nine-tabs__item').attr('aria-hidden', 'true');
        // Toggle class and aria attrs
        jQuery('.nine-tabs[data-id="' + nine_tab_component + '"]').find('.nine-tabs__placeholder').text(nine_tab_txt);
        jQuery(this).addClass('-active').attr('aria-selected','true');
        jQuery('.nine-tabs[data-id="' + nine_tab_component + '"]').find('#' + aria_controls).addClass('-active').attr('aria-hidden','false');
      }
      console.log(aria_controls);
    });
  });
}
nine_tabs();
