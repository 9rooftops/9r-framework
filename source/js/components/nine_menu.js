function nine_menu() {

  // Desktop navigation and keyboard controls
  document.addEventListener('DOMContentLoaded', function () {
    // Open close mobile main menu
    jQuery('body').on('click', '.nine-menu__toggle, .navigation__close-icon', function (event) {
      event.preventDefault();
      jQuery(this).toggleClass('fa-bars fa-times');
      jQuery('.navigation.-main-menu').toggleClass('-active');
      jQuery('body').toggleClass('-active-menu');
      jQuery('.navigation.-main-menu .menu > .menu__item:first-child a').focus();
      // close search if open
      nine_search_close();
    });
    // Escape key to close mobile main menu
    jQuery('body').on('keydown', function (event) {
      if (event.which == '27' && jQuery('.navigation.-main-menu').hasClass('-active')) {
        nine_menu_close();
      }
    });

    const menu_items = document.querySelectorAll('[role="menuitem"]');
    let current_index = 0;

    menu_items[current_index].focus();

    menu_items.forEach((item, index) => {
      item.addEventListener('keydown', (event) => {
        if (event.key === 'ArrowRight') {
          current_index = (index + 1) % menu_items.length;
          menu_items[current_index].focus();
        } else if (event.key === 'ArrowLeft') {
          current_index = (index - 1 + menu_items.length) % menu_items.length;
          menu_items[current_index].focus();
        } else if (event.key === 'Enter' || event.key === ' ') {
          const sub_menu = item.nextElementSibling;
          if (sub_menu && sub_menu.classList.contains('sub-menu')) {
            const is_expanded = item.getAttribute('aria-expanded') === 'true';
            item.setAttribute('aria-expanded', !is_expanded);
            sub_menu.setAttribute('aria-expanded', !is_expanded);
            if (!is_expanded) {
              sub_menu.querySelector('[role="menuitem"]').focus();
            }
          }
        } else if (event.key === 'Escape') {
          const parent_menu = item.closest('.sub-menu');
          if (parent_menu) {
            const parent_menu_item = parent_menu.previousElementSibling;
            parent_menu_item.setAttribute('aria-expanded', 'false');
            parent_menu.setAttribute('aria-expanded', 'false');
            parent_menu_item.focus();
          }
        }
      });
    });
  });
}
// Used to force the menu to close.
// Can also be used from other items like search
function nine_menu_close() {
  // close menu if open
  if (jQuery('.navigation.-main-menu').hasClass('-active')) {
    jQuery('.navigation.-main-menu, .navigation.-main-menu .menu-item, .navigation.-main-menu .sub-menu').removeClass('-active');
    jQuery('.nine-menu__toggle').toggleClass('fa-bars fa-times');
  };
}
nine_menu();
