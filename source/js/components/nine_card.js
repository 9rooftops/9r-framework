var card_height = 0;

function link_is_external(link_element) {
  return (link_element.host !== window.location.host);
}

jQuery('.nine-card__link a').each(function() {
  if (link_is_external(this)) {
    jQuery(this).attr('target', '_blank');
  }
});

function nine_card() {
  jQuery(window).on('load', function () {
    jQuery('.nine-card').on('click', '.nine-card__open-close', function (event) {
      jQuery(this).parent().find('.nine-card__reveal').toggleClass('-active');
      if (jQuery(this).parent().find('.nine-card__reveal').attr('aria-hidden') == 'true') {
        jQuery(this).parent().find('.nine-card__reveal').attr('aria-hidden', 'false');
      }
      else {
        jQuery(this).parent().find('.nine-card__reveal').attr('aria-hidden', 'true');
      }
    });
    // Open and close via spacebar
    jQuery('.nine-card').on('keydown', '.nine-card__open-close', function (key) {
      if (key.which == 32) {
        key.preventDefault();
        jQuery(this).parent().find('.nine-card__reveal').toggleClass('-active');
      }
    });
    // Close via escape
    jQuery('.nine-card').on('keydown', function (key) {
      if (key.which == 27) {
        console.log(key);
        key.preventDefault();
        if (jQuery(this).find('.nine-card__reveal').hasClass('-active')) {
          jQuery(this).find('.nine-card__reveal').toggleClass('-active');
        }
      }
    });
  });
}
nine_card();
