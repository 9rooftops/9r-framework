function nine_is_visible() {
  jQuery(window).on('load', function () {
    // Using JS Intersection Observer instead of jQuery visible
    const animate_observer = new IntersectionObserver((entries) => {

      entries.forEach((entry) => {

        if (entry.isIntersecting) {
          entry.target.classList.add('-is-visible');
        }
        else {
          entry.target.classList.remove('-is-visible');
        }
      });
    });
    const animate_elements = document.querySelectorAll('.-animate');

    // watch and swap classes
    animate_elements.forEach((el) => animate_observer.observe(el));
  });
}
nine_is_visible();
