function nine_accordion() {
  jQuery(window).on('load', function () {
    jQuery('body').on("click", ".nine-accordion .nine-accordion__header", function () {
      var aria_selected = jQuery(this).attr('aria-selected');
      if (aria_selected == 'true') {
        // Toggle class and aria attrs
        jQuery(this).parent().removeClass("-active");
        jQuery(this).attr('aria-selected', 'false');
        jQuery(this).parent().children('.nine-accordion__copy').attr('aria-hidden', 'true');
      }
      else {
        // Close all accordion and reset all aria attrs
        jQuery('.nine-accordion .nine-accordion__item').removeClass('-active');
        jQuery('.nine-accordion .nine-accordion__header').attr('aria-selected', 'false');
        jQuery('.nine-accordion .nine-accordion__copy').attr('aria-hidden', 'true');
        // Toggle class and aria attrs
        jQuery(this).parent().addClass("-active");
        jQuery(this).attr('aria-selected', 'true');
        jQuery(this).parent().children('.nine-accordion__copy').attr('aria-hidden', 'false');
      }
    });
  });
}
nine_accordion();
